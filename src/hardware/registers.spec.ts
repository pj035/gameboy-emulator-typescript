import { Registers } from './registers';

describe('Registers', () => {
  let registers: Registers;

  beforeEach(() => {
    registers = new Registers();
  });

  describe('AF', () => {
    beforeEach(() => {
      // 0011 0001 1010 1010
      registers.AF = 0b0011000110101010;
    });

    it('should return A', () => expect(registers.A).toBe(0x31));

    it('should return F', () => expect(registers.F).toBe(0xaa));

    it('should return z', () => expect(registers.z).toBe(0));

    it('should return n', () => expect(registers.n).toBe(1));

    it('should return h', () => expect(registers.h).toBe(0));

    it('should return c', () => expect(registers.c).toBe(1));
  });

  it('should return B', () => {
    registers.BC = 0xf365;
    expect(registers.B).toBe(0xf3);
  });

  it('should return C', () => {
    registers.BC = 0xf365;
    expect(registers.C).toBe(0x65);
  });

  it('should return D', () => {
    registers.DE = 0xf365;
    expect(registers.D).toBe(0xf3);
  });

  it('should return E', () => {
    registers.DE = 0xf365;
    expect(registers.E).toBe(0x65);
  });

  it('should return H', () => {
    registers.HL = 0xf365;
    expect(registers.H).toBe(0xf3);
  });

  it('should return L', () => {
    registers.HL = 0xf365;
    expect(registers.L).toBe(0x65);
  });
});
