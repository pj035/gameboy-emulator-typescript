/** 16-bit CPU Registers
 *
 * See https://gbdev.io/pandocs/CPU_Registers_and_Flags.html
 */
export class Registers {
  /** Programm counter */
  public PC = 0;
  /** Stack Pointer */
  public SP = 0;
  /** Accumulator & Flags */
  public AF = 0;

  public BC = 0;
  public DE = 0;
  public HL = 0;

  /** High part of AF */
  public get A() {
    const unshifted = this.AF & 0xff00;
    return unshifted >> 8;
  }

  /** Low part of AF */
  public get F() {
    return this.AF & 0x00ff;
  }

  /** Zero flag - 7th bit of F */
  public get z() {
    return (this.F & 0x40) >> 6;
  }

  /** Subtraction flag (BCD) - 6th bit of F */
  public get n() {
    return (this.F & 0x20) >> 5;
  }

  /** Half Carry Flag (BCD) - 5th bit of F */
  public get h() {
    return (this.F & 0x08) >> 4;
  }

  /** Carry Flag - 4th bit of F */
  public get c() {
    return (this.F & 0x08) >> 3;
  }

  /** High part of BC */
  public get B() {
    const unshifted = this.BC & 0xff00;
    return unshifted >> 8;
  }

  /** Low part of BC */
  public get C() {
    return this.BC & 0x00ff;
  }

  /** High part of DE */
  public get D() {
    const unshifted = this.DE & 0xff00;
    return unshifted >> 8;
  }

  /** Low part of DE */
  public get E() {
    return this.DE & 0x00ff;
  }

  /** High part of HL */
  public get H() {
    const unshifted = this.HL & 0xff00;
    return unshifted >> 8;
  }

  /** Low part of HL */
  public get L() {
    return this.HL & 0x00ff;
  }
}
