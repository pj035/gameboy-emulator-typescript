export const MEMORY_SIZE_IN_BYTES = {
  VIDEO: 8192, // 8kB
  RAM: 8192, // 8kB
};

/** Clock Cycles in Hz.  */
export const CLOCK_SPEED = 4194304;

export const SCREEN = {
  /** Resolution in pixel */
  resolution: {
    width: 160,
    height: 144,
  },
};
