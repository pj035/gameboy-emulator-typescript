# gameboy-emulator-typescript

A gameboy emulator (WIP) written in TpeScript

## Sources

* https://gbdev.io/pandocs/
* https://github.com/AntonioND/giibiiadvance/blob/master/docs/TCAGBD.pdf

## Motivation

Out of pure interest. There are "millions" of emulators out there so you would rather use one of them for serious gaming. This one here
is just for exploring the development behind one.

## Related

See also my first step: a (WIP) [Chip-8 emulator](https://gitlab.com/pj035/chip-8-typescript)
## License

See [./LICENSE.txt](./LICENSE.txt)